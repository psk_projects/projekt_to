const Connections = require("./Connection");
let Connection = Connections.Connection;

class InsertData extends Connection {
    constructor() {
        super();
        this.autoIncrement = 0;
    }

    random(data) {
        if (data.length === 1) return 0;
        const len = data.length;
        return Math.floor(Math.random() * len);
    }

    insertData(data) {
        const { table, ilosc, ...newData } = data;
        let value = "";
        let valueSign = "";
        let i = this.autoIncrement;
        this.autoIncrement += ilosc;

        Object.entries(newData).forEach(insert => {
            insert.forEach(item => {
                if (item === insert[0]) {
                    value = value + `${insert[1][this.random(insert[1])]}, `;
                    valueSign = valueSign + `?,`;
                }
            });
        });

        const newValue = value.substring(0, value.length - 2);
        const newValueSign = valueSign.substring(0, valueSign.length - 1);

        this.db.serialize(() => {
            const insert = this.db.prepare(
                `INSERT INTO ${table.toUpperCase()} VALUES (${newValueSign})`
            );
            for (i; i < this.autoIncrement; i++) {
                insert.run(newValue.replace(/autoIncrement/g, i).split(", "));
            }
            insert.finalize();
            console.log(`Trwa wstawianie: ${ilosc} wierszy.`);
        });
    }

    addFromFile(arrs, number, name) {
        let str = "";
        for (let i = 0; i < number; i++) {
            str = str + "?,";
        }
        const newStr = str.substring(0, str.length - 1);
        arrs.forEach((arr, index) => {
            if (index < 100000) {
                const insert = this.db.prepare(
                    `INSERT INTO ${name} VALUES (${newStr})`
                );
                let split = arr.split(",");
                insert.run(
                    index,
                    split[0],
                    split[1],
                    split[2],
                    split[3],
                    split[4],
                    split[5],
                    split[6],
                    split[7],
                    split[8],
                    split[9],
                    split[10],
                    index,
                    index
                );
                insert.finalize();
            } else {
                console.log("Dane wstawione");
                return;
            }
        });
    }
}

module.exports = { InsertData: InsertData };
